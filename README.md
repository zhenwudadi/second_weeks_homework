# 第二周作业

#### 项目介绍
Python基础第二周闯关作业
一、	问题描述：
（本周共计4个必做作业，1个扩展作业）
1). 在命令行模式下登录MySQL数据库，使用SQL语句如下要求：
1. 创建留言数据库: blogdb;
2. 在blogdb数据库中创建会员表users和博客文章表blog，结构如下
 

 3. 在会员表users中添加>=5条的测试数据。
 4. 在blog博文信息表中添加>=10条的测试数据。
 5. 最后将blogdb数据库中的信息导出，并以blogdb.sql文件存储待上交作业。
   2). 如第一题的表结构所示，按下面要求写出对应的SQL语句。
	 1. 在users表中查询注册时间最早的十条会员信息。
	 2. 从两个表中查询点赞数最高的5条博客信息，要求显示字段：
   （博文id，标题，点赞数，会员名）
 3. 统计每个会员的发表博文数量（降序），要求显示字段（会员id号，姓名，博文数量）
 4. 获取会员的博文平均点赞数量最高的三位。显示字段（会员id，姓名，平均点赞数）
 5. 删除没有发表博文的所有会员信息。
   3). 将上周1.10的综合案例《在线学生信息管理

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)