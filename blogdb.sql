/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.1.35-MariaDB : Database - blogdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blogdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `blogdb`;

/*Table structure for table `blog` */

DROP TABLE IF EXISTS `blog`;

CREATE TABLE `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id号',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `abstract` varchar(200) NOT NULL COMMENT '摘要',
  `content` text NOT NULL COMMENT '博文内容',
  `uid` int(10) unsigned DEFAULT NULL COMMENT '用户标识',
  `pcount` int(10) unsigned DEFAULT '0' COMMENT '点赞数',
  `flag` tinyint(3) unsigned DEFAULT '0' COMMENT '状态',
  `cdate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `blog` */

insert  into `blog`(`id`,`title`,`abstract`,`content`,`uid`,`pcount`,`flag`,`cdate`) values (1,'唐嫣罗晋 场地','唐嫣罗晋 场地','24日，配音演员边江的女友晒出一组在奥地利的美照，疑曝光@唐嫣@罗晋 婚礼场地，照片中有开阔的草坪、尖顶教堂，还有山水环绕。新浪娱乐从知情人获悉，唐嫣罗晋确定将于近日在维也纳举行婚礼，作为罗晋好友的边江不知是否要担任伴郎角色呢？[思考]O疑似唐嫣罗晋婚礼场地曝光 ​',1,4472,1,'2018-10-25 00:00:00'),(2,'还珠格格直男投票','还珠格格直男投票','直男投票孰美答案揭晓，第一名晴儿、第二名含香、第三名紫薇、第四名知画、第五名金锁、第六名小燕子，你赞同这份榜单吗？在你心目中又是谁最美呢？ ​',2,5400,1,'2018-10-25 00:00:00'),(3,'去食堂没带饭卡 举报阿姨','去食堂没带饭卡 举报阿姨','据网友爆料，成都一高校女生去食堂没带饭卡，就微信转账给食堂嬢嬢，结果转头就把嬢嬢给举报了（用别的支付会被罚），害得嬢嬢被罚两万，而她得了2000块奖励……[黑线][黑线][黑线]',2,14726,1,'2018-10-25 00:00:00'),(4,'#你不知道的百度APP#','#你不知道的百度APP#','百度App \r\n#王一博撸萌宠#原来形象高冷的@UNIQ-王一博 遇到萌宠秒变王甜甜，看到萌喵忍不住亲亲抱抱举高高，还有抚摸、按摩、健身操！最后无实物撸萌宠的表演充分暴露了猫奴本性，男神的同款大喵打开百度App 即可拥有！#你不知道的百度APP# LFreyaYuezwq6的秒拍视频 ​',1,268,1,'2018-10-25 00:00:00'),(5,'旺仔联名款','旺仔联名款','旺仔联名款礼盒上线！零食与零食的跨界，美味多倍，旺上加旺！超旺的仔们，旺一下吗？转发抽11位送出旺仔联名面膜和雪饼气垫套装！#旺旺special# 预售旺旺special特别礼盒天猫十周年定制跨界零食礼盒赠旺仔面膜 ​',3,1571,1,'2018-10-25 00:00:00'),(6,'亲戚魔鬼式催婚','亲戚魔鬼式催婚','【姑娘遭遇#亲戚魔鬼式催婚# 在家庭群里回了一段话 网友：教科书级别，收藏了！】遭遇亲戚魔鬼式催婚，你会怎么办？近日，杭州一位姑娘就忍不住了，在家庭微信群里，贴出一大段堪称“教科书”般的宣言。还给网友建议，一开始就要反击，否则就是无尽的退让！（杭州日报）O受不了催婚,杭州姑娘在家庭群里发了这样一段话,网友:厉害了!就该这样...你怎么看？ ​',2,3955,1,'2018-10-25 00:00:00'),(7,'陈伟霆 男人的话不可信','陈伟霆 男人的话不可信','打脸狂魔陈伟霆，哈哈哈哈哈哈哈。陈伟霆最新口头禅：男人的话不可信。#陈伟霆 男人的话不可信# #吃瓜姐妹社# ​',1,11483,1,'2018-10-25 00:00:00'),(8,'奥巴马希拉里 爆炸包裹','奥巴马希拉里 爆炸包裹','【重大突发！奥巴马希拉里住所分别发现含爆炸物包裹，索罗斯2天前才受害】据《纽约时报》10月24日报道， 在美国前总统奥巴马和克林顿夫妇的住所分别发现爆炸装置，随后美国特勤局发文，称拦截了送往奥巴马和克林顿夫妇住所的可疑包裹。据悉，这与2天前在亿万富翁索罗斯家中发现的炸弹相似',2,5023,1,'2018-10-25 00:00:00'),(9,'福布斯中国富豪榜','福布斯中国富豪榜','福布斯发布中国400富豪榜！@乡村教师代言人-马云 喜提全国首富！马云自2014年以来首次回归榜首，但财富比一年前缩水40亿美元。去年的首富许家印的排名下降到第3位，@雷军 排名第11位。世界已经将中国与创富联系到一起，中国经济的全球化前所未有！查看完整400人榜单请登录福布斯中文网 ​',3,7013,1,'2018-10-25 00:00:00'),(10,'小学语文教材拼音出错','小学语文教材拼音出错','【有人发抖音称小学语文教材拼音出错，部编本总主编@温儒敏 回应】近日，有网友在抖音上指责部编本语文教材出错，chua和ne拼不出对应的字，是“误人子弟”。对此，24日，部编本语文教材总主编@温儒敏 发博回应网友疑问，Chua的对应有“欻”，拟声词，形容动作迅捷。如“欻的一下就把那张纸撕了”。Ne则对应“哪吒”的“哪”。同时，温儒敏还提出希望大家不要炒作，甚至进行无端的人身攻击',3,8423,1,'2018-10-25 00:00:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id号',
  `name` varchar(32) NOT NULL COMMENT '姓名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱地址',
  `cdate` datetime DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`cdate`) values (1,'jack','jack@hotmail.com','1990-01-01 00:00:00'),(2,'peter','peter@hotmail.com','1976-02-03 00:00:00'),(3,'steven','steven@hotmail.com','2001-11-21 00:00:00'),(4,'fengyun','fengyun@hotmail.com','2011-10-20 00:00:00'),(5,'xiaoming','xiaoming@hotmail.com','1998-03-04 00:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
