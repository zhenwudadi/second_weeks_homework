#导入pymysql模块
import pymysql

#构造方法
class studb:
	def __init__(self):
		self.db = pymysql.connect(host='localhost',user='root',password='',db='mydb',charset='utf8')
		self.cursor = self.db.cursor()
		

	def findall(self):
		try:
			self.cursor.execute(sql)
			if self.cursor.rowcount==0:
				print("========== 没有学员信息可以输出！=============")
			else:
				print("|{0:<5}| {1:<10}| {2:<5}| {3:<10}|".format("sid", "name", "age", "classid"))
				print("-" * 40)
				for i in self.cursor.fetchall():
					print("|{0:<5}| {1:<10}| {2:<5}| {3:<10}|".format(i[0], i[1], i[2],i[3]))
		except Exception as err:
			self.db.rollback()
			print('查询失败！,原因是：', err)

	def insert(self,stu)
		try:
			sql = "insert into stu(name,age,classid)values('%s','%d','%s')"%(stu["name"],stu["age"],stu["classid"])
			self.cursor.execute(sql)
			self.db.commit()
		except Exception as err:
			self.db.rollback()
			print("添加失败！，原因是：",err)

	def delete(self,sid):
		try:
			sql = "delete from stu where id=%d"%(sid)
			self.cursor.execute(sql)
			self.db.commit()
		except Exception as err:
			self.db.rollback()
			print("删除失败！，原因是：",err)

	def __del__(self):
		self.cursor.close()
		self.db.close()

#建立操作类对象
studb=studb()
#程序主循环
while True:
	# 输出初始界面
	print("="*12,"学员管理系统","="*14)
	print("{0:1} {1:13} {2:15}".format(" ","1. 查看学员信息","2. 添加学员信息"))
	print("{0:1} {1:13} {2:15}".format(" ","3. 删除学员信息","4. 退出系统"))
	print("="*40)
	key = input("请输入对应的选择：")
	# 根据键盘值，判断并执行对应的操作
	if key == "1":
		print("="*12,"学员信息浏览","="*14)
		studb.findall()
		input("按回车键继续：")
	elif key == "2":
		print("="*12,"学员信息添加","="*14)
		stu={}
		stu['name']=input("请输入要添加的姓名：")
		stu['age']=int(input("请输入要添加的年龄："))
		stu['classid']=input("请输入要添加的班级号：")
		studb.insert(stu)
		studb.findall()
		input("按回车键继续：")
	elif key == "3":
		print("="*12,"学员信息删除","="*14)
		studb.findall()
		sid = input("请输入你要删除的信息id号：")
		studb.delete(sid)
		studb.findall()
		input("按回车键继续：")
	elif key == "4":
		print("="*12,"再见","="*14)
		break
	else:
		print("======== 无效的键盘输入！ ==========")
